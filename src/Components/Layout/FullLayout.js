import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import ThemeRoutes from '../../Common/router';
import Header from './Header';
import Footer from './Footer';

class FullLayout extends React.Component {
    render() {
        return (
            <div>
                <div>  <Header /> </div>
                <div className="page-wrapper d-block">
                    <div className="page-content container-fluid">
                        <Switch>
                            {ThemeRoutes.map((prop, key) => {
                                if (prop.redirect) {
                                    return <Redirect from={prop.path} to={prop.pathTo} key={key} />;
                                }
                                else {
                                    return (
                                        <Route path={prop.path} component={prop.component} key={key} exact={prop.exact} />
                                    );
                                }
                            })}
                        </Switch>
                    </div>
                    <Footer />
                </div>
            </div>
        )
    }
}
export default FullLayout;