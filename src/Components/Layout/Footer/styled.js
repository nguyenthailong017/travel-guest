import styled from 'styled-components';

export const FooterStyle = styled.div`
    footer {
      background-color: #fffe85;
      height: auto;
      width: 100%;
      display: flex;
      justify-content: center;
      align-items: center;
      padding: 12px;
    }
}
`