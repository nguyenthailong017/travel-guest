import React from 'react';
import { Layout, Menu } from 'antd';
import { NavLink } from 'react-router-dom';
import { HEADERS } from '../../../Constant';
import {
  HeaderStyle,
} from './styled';
const { Header } = Layout;


const Headers = (props) => {

  const menuStyle = {
    flex: 1,    
    display: 'flex', 
    flexDirection: 'row',
    justifyContent: 'center', 
    alignItems: 'center',
    marginLeft: -35,
    backgroundColor: 'black'
  }
  const menuItem = {
    backgroundColor: 'black',
    fontSize: 17,
  }

  return (
    <Layout>
      <HeaderStyle>
        <Header className="header">
          <Menu style={menuStyle} theme="dark" mode="horizontal">
            {
              HEADERS.map((props, key) => {
                return (
                  <Menu.Item style={menuItem} key={key} >
                    <NavLink to={props.path}>
                      <i className={props.icon} />
                    </NavLink>
                  </Menu.Item>
                )
              })
            }
          </Menu>
        </Header>
      </HeaderStyle>
    </Layout>
  )
}
export default Headers;