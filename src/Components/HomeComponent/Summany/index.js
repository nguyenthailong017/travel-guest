import React from 'react'
import { Tabs } from 'antd';
import { TabsStyle, WrapContainer } from './styled';
import SummaryTabs from '../../../Components/Tabs/summany'
const { TabPane } = Tabs;

const Summary = () => {

    return (
        <div>
            <WrapContainer>
                <TabsStyle>
                    <Tabs className="tabs" defaultActiveKey="1">
                        <TabPane tab="summany" key="1">
                            {
                                <SummaryTabs />
                            }
                        </TabPane>
                        <TabPane tab="publications" key="2">
                            Content of Tab Pane 2
                        </TabPane>
                    </Tabs>
                </TabsStyle>
            </WrapContainer>

        </div>
    )
}
export default Summary;