import styled from 'styled-components'


export const WrapContainer = styled.div`
    background-color: #fff;
    width: 100%;
    padding-bottom: 50px;
`;
export const TabsStyle = styled.div`
    margin: 0px auto;
    width: 70%;
`;