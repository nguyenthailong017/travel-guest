import React from 'react'
import ShowMoreText from 'react-show-more-text';

const ReadMore = (props) => {
    const {
        displayLine = 2,
        nameShowMore = '',
        nameShowLess = '',
        className = '',
        onClick,
        expanded = false,
        width = 300,
        content
    } = props;

    return (
        <ShowMoreText
            lines={displayLine}
            more={nameShowMore}
            less={nameShowLess}
            anchorClass={className}
            onClick={onClick}
            expanded={expanded}
            width={width}
        >
            {content}
        </ShowMoreText>
    )
}
export default ReadMore;