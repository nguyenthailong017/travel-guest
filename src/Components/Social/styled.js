import styled from 'styled-components'

export const SocialStyle = styled.div`
    flex: 1;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    padding: 5px;
    margin-top: 10px;
    Button {
        border-radius: 5px;
        margin: 0 30px 0 30px;
        z-index: 1;
    }
`;
export const FollowersStyle = styled.i`
    position: relative;
    top: 0;
    right: 0;
    left: -40px;
    color: #888888;
    border-style:  solid solid solid none;
    padding-left: 20px;
    border-width: thin;
    a {
        flex: 1;
        margin-top: 1px;
        margin-left: -10px;
        padding: 6px;
        font-weight: bold;
        font-style: italic;
    }
`;
