import React from 'react'
import { Button } from 'antd';
import {
    SocialStyle,
    FollowersStyle
} from './styled';

const Social = () => {
    return (
        <SocialStyle>
            <Button
                type="primary"
                icon={<i className="fab fa-facebook" />}
                size="small"
            >
                &#160;Follow
            </Button>
            <FollowersStyle><a href="/">40k</a></FollowersStyle>
            <Button
                style={{ backgroundColor: '#FFCC66' }}
                type="dashed"
                icon={<i className="fab fa-twitter" />}
                size="small"
            >
                &#160;Follow
            </Button>
            <FollowersStyle><a href="/">40k</a></FollowersStyle>
            <Button
                style={{ borderRadius: 0 }}
                type="danger"
                icon={<i className="fab fa-youtube" />}
                size="small"
            >
                &#160;Youtube
            </Button>
            <FollowersStyle><a href="/">40k</a></FollowersStyle>
        </SocialStyle>
    )
}
export default Social;