import styled from 'styled-components'

export const SocialIconStyle = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    padding: 10px;
    font-size: 20px;
    .icon-social>i{
        padding: 0.5em;
        cursor: pointer;
    }
`;
export const AboutQuoteStyle = styled.div`
    padding: 0px 20px;
    color: #bcbcbc;
    width: 60%;
    text-align: center;
    margin: 0 auto;
`;
export const TimeLineStyle = styled.div`
    padding: 10px;
    ul>li{
        margin-bottom: 5px;
    }
`;
export const BiographyStyle = styled.div`
    padding: 10px;
`;