import React from 'react'
import { SocialIconStyle, AboutQuoteStyle, TimeLineStyle, BiographyStyle } from './styled';

const SummanyTabs = () => {
    return(
        <div>
            <SocialIconStyle>
                <div className="icon-social">
                <i className="fas fa-envelope" />
                <i className="fab fa-gitlab" />
                <i className="fab fa-linkedin-in" />
                <i className="fab fa-twitter" />
                <i className="fab fa-facebook-square" />
                <i className="fab fa-soundcloud" />
                <i className="fab fa-youtube" />
                <i className="fas fa-camera" />
                </div>
            </SocialIconStyle>
            <AboutQuoteStyle>
            {`There are no constraints on 
            the human mind, no walls around the 
            human spirit, no barriers to our progress 
            except those we ourselves erect`}
            </AboutQuoteStyle>  
            <BiographyStyle>
                <h1>Biography</h1>
                <div> who helps a team of intelligent 
                    minds in designing cutting-edge 
                    chipsets that millions of people 
                    around the world use in their everyday life. 
                    I use my web development skills (particularly front-end) 
                    to solve day-to-day problems using data and 
                    visualization techniques.</div>
            </BiographyStyle>      
            <TimeLineStyle>
                <h1>Timeline</h1> 
                <ul>
                    <li>
                        <strong>2020-now-</strong>
                        {`Qualcomm - ASIC Physical Design & Signoff, Full-stack Web Development & Machine Learning.`}
                    </li>
                    <li>
                        <strong>2019-now-</strong>
                        {` Nokia - Internship with focus on Automation, Python, Web Development & Machine Learning.`}
                    </li>
                    <li>
                        <strong>2018-2019-</strong>
                        {` Anna University, MIT Campus, Chennai - Master of Engineering in VLSI Design &`}
                    </li>
                    <li>
                        <strong>2017-2018-</strong>
                        {`TATA Consultancy Services - Front-end Web Development, Android Development.`}
                    </li>
                    <li>
                        <strong>2016-2017-</strong>
                        {`Thiagarajar College of Engineering, Madurai - Bachelor of Engineering in Electronics & Communication (CGPA 9.05/10).`}
                    </li>
                    <span> {`In this website, you will find collection of my thoughts, 
                    notes, tutorials and resources based on my experience in technology. 
                    I still learn by myself about the technical topics that 
                    I write here so that I get a clear understanding of it. 
                    I do this mainly during my free time because`} </span>
                     <li>
                        {`It helps me learn these topics better by making me read, write and evaluate myself first before sharing it here.`}
                    </li>
                    <li>
                        {`It provides me a chance to organize my technical interests so that I can refer to it later.`}
                    </li>
                    <li>
                        {`It gives me a chance to share my knowledge with the world where it might help someone somewhere.`}
                    </li>
                </ul>
            </TimeLineStyle>    
        </div>
    )
}
export default SummanyTabs;