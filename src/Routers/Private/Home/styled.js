import styled from 'styled-components'


export const WrapDisplayStyle = styled.div`
    padding: 20px;
`;
export const WrapAvatarStyle = styled.div`
    display: flex;
    height: 150px;
    justify-content: center;
    align-items: center;
`;
export const AvatarStyle = styled.img`
    width: 150px;
    height: 150px;
    border-radius: 50%;
`;
export const WrapInformation = styled.div`
    flex: 1;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    h2 {
        font-weight: bold;
    }
`;
export const SocialStyle = styled.div`
    flex: 1;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    padding: 5px;
    margin-top: 10px;
    Button {
        border-radius: 5px;
        margin: 0 30px 0 30px;
    }
`;