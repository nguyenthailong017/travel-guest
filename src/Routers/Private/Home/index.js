import React from 'react'
import Images from '../../../assets/images'
import Summary from '../../../Components/HomeComponent/Summany'
import Social from '../../../Components/Social'
import {
    WrapDisplayStyle,
    AvatarStyle,
    WrapAvatarStyle,
    WrapInformation,
} from './styled';


const Home = (props) => {
    return (
        <div>
            <WrapDisplayStyle>
                <WrapAvatarStyle>
                    <AvatarStyle src={Images.avatarDefault} alt='avatar' />
                </WrapAvatarStyle>
                <WrapInformation>
                    <h2>Nguyen Thai Long-18I1</h2>
                    <h4>Trường Đại Học Công Nghệ Thông Tin và Truyền Thông Việt - Hàn</h4>
                    <code>eat-sleep-code</code>
                </WrapInformation>
                <Social />
            </WrapDisplayStyle>
            <Summary />
        </div>
    )
}
export default Home;