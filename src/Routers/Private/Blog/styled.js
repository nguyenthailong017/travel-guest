import styled from 'styled-components';


export const WrapDisplayStyle = styled.div`
    padding: 20px;
    background-color: #EEEEEE;
    -webkit-box-shadow: 0px 3px 3px -2px rgba(170,170,170,1);
    -moz-box-shadow: 0px 3px 3px -2px rgba(170,170,170,1);
    box-shadow: 0px 3px 3px -2px rgba(170,170,170,1);

`;
export const WrapInformation = styled.div`
    h1 {
        font-weight: bold;
    }
    flex: 1;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    text-align: center;
    code {
        width: 40%;
        font-size: 15px;
    }
    padding: 20px 0 20px 0;
`;
export const WrapCategoryStyle = styled.div`
    padding: 10px 200px;
    .wrap-cards{
        flex: 1;
        display: flex;
        flex-direction: row;
        justify-content: center;
        align-items: center;
    }
    .card-items{
        margin-bottom: 15px;
    }
`;
export const ImagesStyle = styled.img`
    max-height: 270px;
    background-size: cover;
    padding: 10px;

    &:hover {
        opacity: 1;
        -webkit-animation: flash 1s;
        animation: flash 1s;
      }
      
      @-webkit-keyframes flash {
        0% {  opacity: .3;  }
        100% { opacity: 1; }
      }
      @keyframes flash {
        0% { opacity: .3; }
        100% { opacity: 1;  }
      }
      
`;
export const MetaStyle = styled.div`
      margin-top: -10px;
      text-align: center;
      font-style: oblique;
`;