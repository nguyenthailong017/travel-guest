import React from 'react'
import Social from '../../../Components/Social'
import { Row, Col, Card } from 'antd';
import Image from '../../../assets/images'
import {
    WrapDisplayStyle,
    WrapInformation,
    WrapCategoryStyle,
    ImagesStyle,
    MetaStyle
} from './styled';
import ReadMore from '../../../Components/ReadMore'
import { ROUTERS } from '../../../Constant';
const { Meta } = Card;

const style = { backgroundColor: '#EEEEEE', padding: '8px 0' };

const Blog = (props) => {

    const handleMove = (routerName) => {
        props.history.push(routerName)
    }
    return (
        <div>
            <WrapDisplayStyle>
                <WrapInformation>
                    <h1>Blog</h1>
                    <code>
                        {`You have found yet another learning space 
                        in this universe where you got a chance to 
                        learn something useful along with me. 
                        Come let's learn & explore the world together 😇`}
                    </code>
                </WrapInformation>
                <Social />
            </WrapDisplayStyle>
            <WrapCategoryStyle>
                <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }} className="wrap-cards">
                    <Col span={6} className="card-items">
                        <Card
                            onClick={() => handleMove(ROUTERS.CATEGORY)}
                            hoverable
                            cover={<ImagesStyle alt="tralvel" src={Image.tralvel1} />}
                            style={style}>
                            <MetaStyle>
                                <Meta
                                    title={<h3><strong>{`Europe Street beat`}</strong></h3>}
                                    description={
                                        <ReadMore content={`The standard chunk of 
                                            Lorem Ipsum used since the 1500s 
                                            is reproduced below for those interested.`}
                                        />}
                                />
                            </MetaStyle>
                        </Card>
                    </Col>
                    <Col span={6} className="card-items">
                        <Card
                            onClick={() => handleMove(ROUTERS.CATEGORY)}
                            hoverable
                            cover={<ImagesStyle alt="tralvel" src={Image.tralvel1} />}
                            style={style}>
                            <MetaStyle>
                                <Meta
                                    title={<h3><strong>{`Europe Street beat`}</strong></h3>}
                                    description={
                                        <ReadMore content={`The standard chunk of 
                                            Lorem Ipsum used since the 1500s 
                                            is reproduced below for those interested.`}
                                        />}
                                />
                            </MetaStyle>
                        </Card>
                    </Col>
                    <Col span={6} className="card-items">
                        <Card
                            onClick={() => handleMove(ROUTERS.CATEGORY)}
                            hoverable
                            cover={<ImagesStyle alt="tralvel" src={Image.tralvel1} />}
                            style={style}>
                            <MetaStyle>
                                <Meta
                                    title={<h3><strong>{`Europe Street beat`}</strong></h3>}
                                    description={
                                        <ReadMore content={`The standard chunk of 
                                            Lorem Ipsum used since the 1500s 
                                            is reproduced below for those interested.`}
                                        />}
                                />
                            </MetaStyle>
                        </Card>
                    </Col>
                    <Col span={6} className="card-items">
                        <Card
                            onClick={() => handleMove(ROUTERS.CATEGORY)}
                            hoverable
                            cover={<ImagesStyle alt="tralvel" src={Image.tralvel1} />}
                            style={style}>
                            <MetaStyle>
                                <Meta
                                    title={<h3><strong>{`Europe Street beat`}</strong></h3>}
                                    description={
                                        <ReadMore content={`The standard chunk of 
                                            Lorem Ipsum used since the 1500s 
                                            is reproduced below for those interested.`}
                                        />}
                                />
                            </MetaStyle>
                        </Card>
                    </Col>
                    <Col span={6} className="card-items">
                        <Card
                            onClick={() => handleMove(ROUTERS.CATEGORY)}
                            hoverable
                            cover={<ImagesStyle alt="tralvel" src={Image.tralvel1} />}
                            style={style}>
                            <MetaStyle>
                                <Meta
                                    title={<h3><strong>{`Europe Street beat`}</strong></h3>}
                                    description={
                                        <ReadMore content={`The standard chunk of 
                                            Lorem Ipsum used since the 1500s 
                                            is reproduced below for those interested.`}
                                        />}
                                />
                            </MetaStyle>
                        </Card>
                    </Col>
                    <Col span={6} className="card-items">
                        <Card
                            onClick={() => handleMove(ROUTERS.CATEGORY)}
                            hoverable
                            cover={<ImagesStyle alt="tralvel" src={Image.tralvel1} />}
                            style={style}>
                            <MetaStyle>
                                <Meta
                                    title={<h3><strong>{`Europe Street beat`}</strong></h3>}
                                    description={
                                        <ReadMore content={`The standard chunk of 
                                            Lorem Ipsum used since the 1500s 
                                            is reproduced below for those interested.`}
                                        />}
                                />
                            </MetaStyle>
                        </Card>
                    </Col>
                </Row>
            </WrapCategoryStyle>
        </div>
    )
}
export default Blog;