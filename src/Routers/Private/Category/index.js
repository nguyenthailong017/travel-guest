import React from 'react';
import { Row, Col, Card, Button } from 'antd';
import Image from '../../../assets/images'
import {
    WrapContainer,
    ImageStyle,
    TitleStyle,
    TextStyle,
    HrStyle,
    TitleContentStyle,
    TitleCategoriesStyle,
    HrStyleCustom,
    HrHeaderStyle,
    ImageItemContentStyle,
    WrapContentStyle,
    InteractiveStyle,
    AvatarSquareStyle,
    WrapAvatarSquareStyle,
    AvatarCyrclerStyle,
    WrapAvatarCyrclerStyle,
    WrapInteractiveImagesStyle,
    HrStyleQuote,
    QuoteStyle,
    FollowersStyle
} from './styled';


const Category = () => {
    return (
        <WrapContainer>
            <Row gutter={[8, 8]}>
                <Col span={18}>
                    <HrHeaderStyle />
                    <ImageStyle src={Image.tralvel2} />
                    <TitleStyle>
                        <span className="title">
                            <strong>#10</strong><code>Travel</code>
                        </span>
                        <TextStyle>
                            <h2>"Neque porro quisquam"</h2>
                            <code>"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...""</code>
                        </TextStyle>
                    </TitleStyle>
                    <HrStyle />
                    <span>
                        {`Lorem Ipsum is simply dummy text 
                    of the printing and typesetting industry. 
                    Lorem Ipsum has been the industry's standard 
                    dummy text ever since the 1500s, when an 
                    unknown printer took a galley of type and 
                    scrambled it to make a type specimen book. 
                    It has survived not only five centuries, 
                    but also the leap into electronic typesetting, 
                    remaining essentially unchanged. It was popularised 
                    in the 1960s with the release of Letraset sheets 
                    containing Lorem Ipsum passages, and more recently 
                    with desktop publishing software like Aldus PageMaker 
                    including versions of Lorem Ipsum`}
                    </span>
                    <TitleCategoriesStyle>
                        <h2><span>Lorem Ipsum Lorem Ipsum </span></h2>
                    </TitleCategoriesStyle>
                    <TitleContentStyle><strong> {`#1`} </strong> {`_Lorem Ipsum Lorem Ipsum`} </TitleContentStyle>
                    <HrStyleCustom />
                    <ImageItemContentStyle src={Image.tralvel3} alt="image item content" />
                    <WrapContentStyle>
                        <p>
                            {`Lorem Ipsum is simply dummy text 
                    of the printing and typesetting industry. 
                    It has survived not only five centuries, 
                    but also the leap into electronic typesetting, 
                    remaining essentially unchanged. It was popularised 
                    in the 1960s with the release of Letraset sheets 
                    containing Lorem Ipsum passages, and more recently 
                    with desktop publishing software like Aldus PageMaker 
                    including versions of Lorem Ipsum`}
                        </p>
                        <p>
                            {`Lorem Ipsum is simply dummy text 
                    of the printing and typesetting industry. 
                    It was popularised in the 1960s with the release of Letraset sheets 
                    containing Lorem Ipsum passages, and more recently 
                    with desktop publishing software like Aldus PageMaker 
                    including versions of Lorem Ipsum`}
                        </p>
                        <p>
                            {`Lorem Ipsum is simply dummy text 
                    of the printing and typesetting industry:`}
                        </p>
                        <ul>
                            <li>
                                <strong>Lorem Ipsum is simply dummy text</strong><br />
                                {`Lorem Ipsum is simply dummy text 
                            of the printing and typesetting industry. 
                            It was popularised in the 1960s with the release of Letraset sheets 
                            containing Lorem Ipsum passages, and more recently 
                            with desktop publishing software like Aldus PageMaker 
                            including versions of Lorem Ipsum`}
                            </li>
                            <li>
                                <strong>Lorem Ipsum is simply dummy text</strong><br />
                                {`Lorem Ipsum is simply dummy text 
                            of the printing and typesetting industry. 
                            and more recently 
                            with desktop publishing software like Aldus PageMaker 
                            including versions of Lorem Ipsum`}
                            </li>
                            <li>
                                <strong>Lorem Ipsum is simply dummy text</strong>
                                <Row gutter={24}>
                                    <Col span={8}>
                                        <Card
                                            className="item-images"
                                            cover={<img alt="example" src={Image.tralvel1} />}
                                        />
                                    </Col>
                                    <Col span={8}>
                                        <Card
                                            className="item-images"
                                            cover={<img alt="example" src={Image.tralvel1} />}
                                        />
                                    </Col>
                                    <Col span={8}>
                                        <Card
                                            className="item-images"
                                            cover={<img alt="example" src={Image.tralvel1} />}
                                        />
                                    </Col>
                                </Row>
                                <p> {`This is the market square that is deemed as the largest in Europe at roughly 40,000 m2! The landmark highlights of this place would be:`} </p>
                                <ul>
                                    <li>
                                        <strong> {`Lorem Ipsum is simply dummy text-`} </strong>
                                        {`Lorem Ipsum is simply dummy text 
                                    of the printing and typesetting industry. 
                                    It was popularised in the 1960s with the release of Letraset sheets 
                                    containing Lorem Ipsum passages, and more recently 
                                    with desktop publishing software like Aldus PageMaker 
                                    including versions of Lorem Ipsum`}
                                    </li>
                                    <li>
                                        <strong> {`Lorem Ipsum is simply dummy text-`} </strong>
                                        {`Lorem Ipsum is simply dummy text 
                                    of the printing and typesetting industry. 
                                    and more recently 
                                    with desktop publishing software like Aldus PageMaker 
                                    including versions of Lorem Ipsum`}
                                    </li>
                                    <li>
                                        <strong> {`Lorem Ipsum is simply dummy text-`} </strong>
                                        {`Lorem Ipsum is simply dummy text 
                                    of the printing and typesetting industry. 
                                    It was popularised in the 1960s with the release of Letraset sheets 
                                    containing Lorem Ipsum passages, and more recently 
                                    with desktop publishing software like Aldus PageMaker 
                                    including versions of Lorem Ipsum`}
                                    </li>
                                    <li>
                                        <strong> {`Lorem Ipsum is simply dummy text-`} </strong>
                                        {`Lorem Ipsum is simply dummy text 
                                    of the printing and typesetting industry. 
                                    and more recently 
                                    with desktop publishing software like Aldus PageMaker 
                                    including versions of Lorem Ipsum`}
                                    </li>
                                    <QuoteStyle>
                                        <HrStyleQuote />
                                        <div className="quote">
                                            <i className="fas fa-quote-left" />
                                            <code>
                                                {`There’s an interesting tale about 
                                    this dragon that helped brought forth 
                                    the name of the city. As the stories 
                                    have it, Smok found joy in eating sheep 
                                    and young local girls. Every attempt of 
                                    killing him had always been unsuccessful… 
                                    until a poor cobbler named Krak made Smok 
                                    eat a sheep injected with sulphur. This 
                                    eventually made Smok explode at some point 
                                    after he drank some water. As expected, 
                                    Krak was given the honor of marrying the 
                                    city’s princess because of his gallant 
                                    act — which then made him king. To possibly 
                                    savor his victory, he built his castle above the 
                                    dragon’s home, which made the citizens build a city 
                                    around it and then calling it after their king: Krakow.`}
                                            </code>
                                        </div>
                                    </QuoteStyle>
                                </ul>
                            </li>
                        </ul>
                    </WrapContentStyle>

                </Col>
                <Col span={6}>
                    <InteractiveStyle>
                        <div>
                            <h2>SUBSCRIBE</h2>
                            <WrapAvatarSquareStyle>
                                <AvatarSquareStyle src={Image.avatarDefault} alt="interactive-avatar" />
                                <div className="child">
                                    <code>Viet Nam Travel</code>
                                    <button><i className="fab fa-youtube">&nbsp;YouTube</i></button><FollowersStyle><a href="/">40k</a></FollowersStyle>
                                </div>
                            </WrapAvatarSquareStyle>
                        </div>
                        <div>
                            <h2>ABOUT</h2>
                            <WrapAvatarCyrclerStyle>
                                <AvatarCyrclerStyle src={Image.avatarDefault} alt="interactive-avatar" />
                                <span>
                                    {`Hey there! I am Aileen Adalid.
                                At 21, I quit my corporate job in 
                                the Philippines to pursue my dreams. 
                                Today, I am a successful digital nomad 
                                (entrepreneur, travel writer, & vlogger) 
                                living a sustainable travel lifestyle.`}
                                </span>
                                <span>
                                    {`My mission? To show you how it is 
                                absolutely possible to create a life 
                                of travel too (no matter the odds), 
                                and I will help you achieve that through 
                                my detailed travel guides, adventures, 
                                resources, tips, and MORE!`}
                                </span>
                            </WrapAvatarCyrclerStyle>
                        </div>
                        <div>
                            <h2>INSTAGRAM</h2>
                            <WrapInteractiveImagesStyle>
                                <Row gutter={24}>
                                    <Col span={12}>
                                        <Card
                                            className="interactive-item-images"
                                            cover={<img alt="example" src={Image.tralvel1} />}
                                        />
                                    </Col>
                                    <Col span={12}>
                                        <Card
                                            className="interactive-item-images"
                                            cover={<img alt="example" src={Image.tralvel1} />}
                                        />
                                    </Col>
                                    <Col span={12}>
                                        <Card
                                            className="interactive-item-images"
                                            cover={<img alt="example" src={Image.tralvel1} />}
                                        />
                                    </Col>
                                    <Col span={12}>
                                        <Card
                                            className="interactive-item-images"
                                            cover={<img alt="example" src={Image.tralvel1} />}
                                        />
                                    </Col>
                                    <Col span={24} className="button-instagram">
                                        <Button type="primary" ><i className="fab fa-instagram" />&nbsp;Follow Instagram</Button>
                                    </Col>
                                </Row>
                            </WrapInteractiveImagesStyle>
                        </div>
                    </InteractiveStyle>
                </Col>
            </Row>
        </WrapContainer>
    )
}
export default Category;