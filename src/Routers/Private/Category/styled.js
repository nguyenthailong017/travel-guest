import styled from 'styled-components';

export const WrapContainer = styled.div`
    margin: 0;
    padding: 26px 46px;
`;
export const ImageStyle = styled.img`
    -webkit-box-shadow: 0px 0px 2px 1px rgba(0,0,0,0.75);
    -moz-box-shadow: 0px 0px 2px 1px rgba(0,0,0,0.75);
    box-shadow: 0px 0px 2px 1px rgba(0,0,0,0.75);
    width: 990px; 
    max-height: 350px; 
    object-fit: cover;
`;
export const TitleStyle = styled.div`
    padding: 16px 0;

    .title {
        height: 80px; 
        max-width: 45px; 
        background-color: #ed2665;
        flex: 1;
        display: flex; 
        flex-direction: column; 
        justify-content: center; 
        align-items: center;
    }
    strong {
        font-size: 20px;
    }
    span {
    algin-items: center;
    font-size: 10px;
    }
`;
export const TextStyle = styled.div`
    position: relative; 
    top: 0; 
    right: 0; 
    left: 0;
    bottom: 0;
    h2 {
        position: absolute; 
        top: -65px; 
        right: 0; 
        left: 60px;
        font-weight: bold;
    }
    code {
        position: absolute;
        top: -33px; 
        right: 0; 
        left: 60px;
        font-style: italic;
    }
`;

export const HrHeaderStyle = styled.hr`
    padding: 10px 0;
    width: 1320px;
    color: #DDDDDD;
    border-style: solid none none none;
`;

export const HrStyle = styled.hr`
    padding: 16px 0;
    color: #DDDDDD;
    border-style: solid none none none;
`;
export const HrStyleCustom = styled.hr`
    padding: 2px 0;
    color: #CCCCCC;
    border-style: dashed none none none;
`;
export const HrStyleQuote = styled.hr`
    padding: 0.5px 0;
    color: #ed2665;
    font-size: 6px;
    border-style: solid none none none;
    background-color: #ed2665;
`;
export const TitleCategoriesStyle = styled.div`
    h2{
        margin: 50px auto;
        text-align: center;
        max-width: 600px;
        position: relative;
        display: flex;
        flex-wrap: wrap;
        justify-content: center;
        align-items: center;
    }
   
    h2::before {
        content: "";
        width: 500px;
        height: 1px;
        background-color: #ed2665;
        position: absolute;
        right: 50%;
        top: 50%;
        z-index: -1;
    }
    h2::after {
        content: "";
        width: 500px;
        height: 1px;
        background-color: #ed2665;
        position: absolute;
        left: 50%;
        top: 50%;
        z-index: -1;
    }
    span {
        padding: 16px 26px;
        border-style: solid;
        border-width: thin;
        color: #ed2665;
        background-color: white;
        z-index: 1;
    }
`;
export const TitleContentStyle = styled.h3`
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 22px;
    font-style: oblique;
    strong {
        color: #ed2665;
    }
`;
export const ImageItemContentStyle = styled.img`
    padding: 16px 0;
    width: 990px; 
    max-height: 420px; 
    object-fit: cover;
`;
export const WrapContentStyle = styled.div`
    padding: 10px 0;
    p {
        font-size: 16px;
    }
    ul {
        li {
            margin-bottom: 16px;
            font-size: 16px;
            strong {
                font-style: italic;
                text-decoration: underline;
                font-size: 18px;
            }
        }
    }
    .item-images {
        border: none;
        img {
            width: 280px;
            max-height: 180px;
            object-fit: cover;
        }
    }
`;

export const InteractiveStyle = styled.div`
    margin-left: 26px;
    h2{
        margin: 36px auto;
        text-align: center;
        position: relative;
        display: flex;
        flex-wrap: wrap;
        justify-content: center;
        align-items: center;
    }

    h2::before {
        content: "";
        width: 60px;
        height: 1px;
        background-color: #ed2665;
        position: absolute;
        right: 0;
        top: 50%;
        z-index: -1;
    }
    h2::after {
        content: "";
        width: 60px;
        height: 1px;
        background-color: #ed2665;
        position: absolute;
        left: 0;
        top: 50%;
        z-index: -1;
    }
`;
export const AvatarSquareStyle = styled.img`
    vertical-align: middle;
    width: 66px;
    height: 66px;
    margin-left: 36px;
`;
export const WrapAvatarSquareStyle = styled.div`
    padding: 16px 16px;
    .child{
        display: flex; 
        flex-direction: column; 
        justify-content: center; 
        align-items: center;
        position: absolute;
        top: 130px;
        left: 105px;
        right: 0;
            code {
                padding: 6px 6px;
            }
            button {
                border: none;
                background-color: red;
                margin-left: -42px;
                i {
                    display: flex;
                    height: 22px;
                    align-items: center;
                    color: white;
                }
            }
    }
`;
export const FollowersStyle = styled.i`
    position: relative;
    top: -24px;
    right: 0;
    left: 36px;
    height: 24px;
    color: #888888;
    border-style:  solid solid solid none;
    border-width: thin;
    background-color: #DDDDDD;
    a {
        flex: 1;
        margin-top: 1px;
        padding: 6px;
        font-weight: bold;
        color: #333333;
        &:hover {
            color: blue;
        }
    
    }
`;
export const AvatarCyrclerStyle = styled.img`
    vertical-align: middle;
    width: 160px;
    height: 160px;
    border-radius: 50%;
`;
export const WrapAvatarCyrclerStyle = styled.div`
    padding: 16px 16px;
    display: flex; 
    flex-direction: column; 
    justify-content: center; 
    align-items: center;
    span {
        font-size: 16px;
        font-weight: none;
        text-align: center;
    }
`;
export const WrapInteractiveImagesStyle = styled.div`
    padding: 46px 0 ;
    .interactive-item-images {
        border: none;
        border-radius: none;
        z-index: 2;
        img {
           width: 154px; 
           height: 154px;
           margin-top: -42px;
        }
    }
    .button-instagram {
        margin-top: -26px;
        z-index: 9999;
        display: flex;
        justify-content: center;
        align-items: center;
    }
`;
export const QuoteStyle = styled.div`
    background-color: #ededed;
    
    .quote {
        padding: 6px 16px;
        i{
            margin-left: 16px;
        }
        code {
            text-align: center;
            margin-left: 6px;
        }
    }
`;
