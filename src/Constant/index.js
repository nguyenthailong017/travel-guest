
export const ROUTERS = {
    LOGIN: '/login',
    HOME: '/home',
    BLOG: '/blog',
    CATEGORY: '/category',
    NONE: '/'
}
export const HEADERS = [
    {
        path: ROUTERS.LOGIN,
        icon: '',
    },
    {
        path: ROUTERS.HOME,
        icon: 'fas fa-home',
    },
    {
        path: ROUTERS.BLOG,
        icon: 'fas fa-pencil-alt',
    },
    {
        path: ROUTERS.NONE,
        icon: 'fas fa-headphones',
    },
    {
        path: ROUTERS.NONE,
        icon: 'fas fa-sun',
    },
]