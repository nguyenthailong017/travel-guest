import {
    REMOVE_TOKEN,
    LOGIN_SUCCESS,
    CHECK_LOGIN_SUCCESS
} from '../constants'

const INIT_STATE = {
    token: null,
    username: '',
    email: '',
    role: ''
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case REMOVE_TOKEN:
            return {
                ...state,
                token: action.payload
            }
            case CHECK_LOGIN_SUCCESS:
                // const { token, username, email, role } = action.payload
                return {
                    ...state,
                    // token,
                    // username,
                    // email,
                    // role
                }
        case LOGIN_SUCCESS:
            const { token, username, email, role } = action.payload
            return {
                ...state,
                token,
                username,
                email,
                role
            }
       
        default:
            return state;
    }
}