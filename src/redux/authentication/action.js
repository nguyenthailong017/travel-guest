import { 
    REMOVE_TOKEN,
    LOGIN_SUCCESS,
    CHECK_LOGIN_SUCCESS 
} from '../constants'

export const removeToken = (payload) => {
    return {
        type: REMOVE_TOKEN,
        payload
    }
}

export const checkLoginSuccess = (payload) => {
    return {
        type: CHECK_LOGIN_SUCCESS,
        payload
    }
}

export const loginSuccess = (payload) => {
    return {
        type: LOGIN_SUCCESS,
        payload
    }
}