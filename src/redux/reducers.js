import { combineReducers } from 'redux';
import settings from './settings/reducer';
import authentication from './authentication/reducer';

const reducers = combineReducers({
    settings,
    authentication
});

export default reducers;