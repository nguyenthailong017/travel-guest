import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
// import { I18nextProvider } from 'react-i18next';
// import i18next from 'i18next';
import PRIVATE_ROUTES from './Common';
import { PrivateRoute } from './Common/PrivateRouters';
import { configureStore } from './redux/store';
import BlankLayout from './Components/Layout/BlankLayout';
// import { ROUTERS } from './Common/Constants';
import { history } from './Services/History';
// import en from './Common/Language/en/index.json';
// import vi from './Common/Language/vi/index.json';

// i18next.init({
//   interpolation: { escapeValue: false }, // React already does escaping
//   lng: 'en', // language to use
//   resources: {
//     en,
//     vi
//   }
// });
class App extends React.PureComponent {
  render() {
    return (
      <Provider store={configureStore()}>
        {/* <I18nextProvider i18n={i18next}> */}
          <Router basename="/" history={history}>
            <Switch>
              <Route path="/login" component={BlankLayout} />;
              {PRIVATE_ROUTES.map((props, key) => {
                console.log({props});
                return (
                  <PrivateRoute
                    path={props.path}
                    component={props.component}
                    key={key}
                  />
                );
              })}
            </Switch>
          </Router>
        {/* </I18nextProvider> */}
      </Provider>
    );
  }
}

export default App;