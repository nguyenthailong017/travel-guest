import React from 'react';
import { Route } from 'react-router-dom';
// import { authenticationService } from '../../Services';
// import { ROUTERS } from '../Constants';

export const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => <Component {...props} />} />
);

export default PrivateRoute;
