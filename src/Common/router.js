import { ROUTERS } from '../Constant';
import Home from '../Routers/Private/Home';
import Blog from '../Routers/Private/Blog';
import Category from '../Routers/Private/Category';

var ThemeRoutes = [
    {
		path: ROUTERS.HOME,
		component: Home,
		exact: true
	},
	{
		path: ROUTERS.BLOG,
		component: Blog,
		exact: true
	},
	{
		path: ROUTERS.CATEGORY,
		component: Category,
		exact: true
    },
    { path: '/', pathTo: ROUTERS.HOME, name: 'Home', redirect: true }
]
export default ThemeRoutes;
