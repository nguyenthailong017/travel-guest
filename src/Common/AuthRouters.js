import Login from '../Routers/Public/Login';
// import { ROUTERS } from '../Constants';

const authRoutes = [
    { path: '/login' , name: 'Login', icon: 'mdi mdi-account-key', component: Login }
];
export default authRoutes;
