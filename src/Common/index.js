import FullLayout from '../Components/Layout/FullLayout';

const indexRoutes = [{ path: '/', name: '/Home', component: FullLayout }];

export default indexRoutes;
